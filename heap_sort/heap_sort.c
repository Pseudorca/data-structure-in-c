#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

bool compareMax(int a, int b) {
	return (a>b)?true:false;
}

bool compareMin(int a, int b) {
	return (a<b)?true:false;
}

void swap(int* a, int* b) {
	int tmp = *a;
	*a = *b;
	*b = tmp;
}

void reHeapUp(int* heap, int newNode, bool comp(int, int)) {
	while(newNode != 0) {
		int parent = (newNode+1)/2-1;
		if(comp(heap[parent], heap[newNode]))
			break;
		swap(&heap[parent], &heap[newNode]);
		newNode = parent;
	}
}

void reHeapDown(int * heap, int root, int last, bool comp(int, int)) {
	while((root+1)*2-1 <= last) {
		int left = (root+1)*2-1, right = (root+1)*2;
		if(right <= last) {
			if(comp(heap[root], heap[left]) && comp(heap[root], heap[right]))
				break;
			if(comp(heap[left], heap[right])) {
				swap(&heap[root], &heap[left]);
				root = left;
			} else {
				swap(&heap[root], &heap[right]);
				root = right;
			}
		} else if(comp(heap[left], heap[root])) {
			swap(&heap[root], &heap[left]);
			root = left;
		} else
			break;
	}
}

void printHeap(int* heap, int len) {
	for(int i = 0; i < len; i++)
		printf("%d ", heap[i]);
	printf("\n");
}

void createHeap(int* heap, int len, bool comp(int, int)) {
	for(int i = 1; i < len; i++) {
		reHeapUp(heap,i,comp);
		printHeap(heap,len);
	}
}

void heapSort(int* heap, int len, bool comp(int, int)){
	for(int i = 1; i < len; i++) {
		swap(&heap[0], &heap[len-i]);
		reHeapDown(heap,0,(len-i)-1,comp);
		printHeap(heap,len);
	}
}

void main() {
	while(true){
		int *heap;
		int i, len, input;

		printf("%s: ", "Input Length");
		scanf("%d", &len);
		if(len == -1)
			break;
		else
			heap = (int *)malloc(sizeof(int)*len);

		printf("%s: ", "Input random seed: ");
		scanf("%d", &input);
		srand(input);
		while(i<len) {
			int r = (rand() % 100)+1, j;
			for(j=0; j<i; j++) {
				if(heap[j] == r)
					break;
			}
			if(i == j)
				heap[i++] = r;
		}
		printf("%s: ", "Input: (1)small-to-big, (2)big-to-small");
		scanf("%d", &input);
		
		printf("\n=== Origin ===\n");
		printHeap(heap, len);

		printf("\n=== createHeap ===\n");
		(input == 1)?createHeap(heap, len, compareMax):createHeap(heap, len, compareMin);

		printf("\n=== heapSort ===\n");
		(input == 1)?heapSort(heap, len, compareMax):heapSort(heap, len, compareMin);
		printf("\n");
		free(heap);
	}
	// system("pause");
	return;
}
