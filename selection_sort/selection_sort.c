#include <math.h>
#include <stdio.h>
#include <stdlib.h>

// swap as function
void swap(int *x, int *y) {
	int tmp = *x;
	*x = *y;
	*y = tmp;
}
void sort(int [], int);
// swap as macro
#define SWAP(x, y, t) ((t) = (x), (x) = (y), (y) = (t))
#define MAX_SIZE 101

int main() {
	int i, n;
	int list[MAX_SIZE];

	printf("%s", "Enter a number to generate: ");
	scanf("%d", &n);
	if(n < 1 || n > MAX_SIZE) {
		fprintf(stderr, "Imporper value of n\n");
		exit(EXIT_FAILURE);
	}
	for(i=0; i<n; i++) {
		list[i] = rand() % 1000;
		printf("%d ", list[i]);
	}
	sort(list, n);
	printf("\n%s\n", "Sorted array: ");
	for(i=0; i<n; i++)
		printf("%d ", list[i]);
	printf("\n");
	
	return 0;
}

void sort(int list[], int n) {
	int i, j, min, tmp;
	for(i=0; i<(n-1); i++) {
		min = i;
		for(j=i+1; j<n; j++) {
			if(list[j] < list[min])
				min = j;
		}
		swap(&list[i], &list[min]);
	}
}
