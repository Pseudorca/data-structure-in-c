﻿##利用Linked List 以及dynamic memory allocation完成下列程式設計
---
- 給定一檔案“ input.txt”，檔案中包含貨品的名稱及價格，檔案中貨品的名稱及價格皆不相同且每筆資料佔據檔案一行。寫一程式依序由該檔案中讀取一筆資料並將之串入linked list中，該linked list 中的節點無論於任何時刻皆須保持由小到大的順序，因此每次由檔案中讀取一個新資料後程式必須使用dynamic memory allocation 產生新的節點，並將該節點插入linked list中並按價格由小到大的順序。
- 此外程式需提供一使用者介面，該介面提供使用者print list 、delete element、與exit 的功能:
	1. print list : 由頭至尾列印該linked list 內容 (所以貨品應該由小至大排列)
	2. delete element : 讓使用者輸入一貨品名稱，程式於linked list 中尋找該貨品，若該貨品存在則將該貨品刪除並印出“delete success”， 若該貨品不存在則列印“not exist”訊息。
	3. exit : 釋放所有動態宣告的記憶體之後結束程式
- Linked List使用array建立則不予計分，程式中沒有按程式原型(以下程式碼)編寫則不予計分
