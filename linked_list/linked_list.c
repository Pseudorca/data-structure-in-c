#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct node {
	char name[20];
	int price;
	struct node* link;
} NODE;

int insert_element (NODE** head, char name[], int price) {
	NODE *node = (NODE*)malloc(sizeof(NODE));
	if(node == NULL) {
		return 2;
	}

	strcpy(node->name, name);
	node->price = price;
	node->link = NULL;

	if(*head == NULL) {
		// set the first data
		*head = node;
	} else if(*head != NULL) {
		// compare price with price in the first node
		if(price < (*head)->price) {
			node->link = *head;
			*head = node;
		} else {
			NODE *now = *head;
			while(now != NULL && price > now->price) {
				// check next element existance, than price
				if((now->link) != NULL && price > (now->link)->price) {
					// exist & node->price > next element's, update 'now' element
					now = now->link;
				} else {
					// not exist or node->price < next element's
					node->link = now->link;
					now->link = node;
					break;
				} 
			}
		}
	}
	return 1;
}

int delete_element (NODE** head, char name[]) {
	NODE *del, *now = *head;
	if(*head != NULL && !strcmp(name, (*head)->name)) {
		del = *head;
		*head = del->link;
		free(del);
		return 1;
	} else if(*head != NULL) {
		while((now->link) != NULL) {
			if(!strcmp(name, (now->link)->name)) {
				del = now->link;
				now->link = (now->link)->link;
				free(del);
				return 1;
			}
			now = now->link;
		}
	}
	return 2;
}

void print_list (NODE* head) {
	NODE *now = head;
	while(now != NULL) {
		printf("%s\t%d\n", now->name, now->price);
		now = now->link;
	}
}

void destory_list(NODE** head) {
	NODE *del = NULL;
	while(*head != NULL) {
		del = *head;
		*head = del->link;
		free(del);
	}
}

void main() {
	NODE* list = NULL;
	char name[20];
	int price, input;
	// read product name and price in 'input.txt'
	FILE *file = fopen("input.txt", "r");
	while(fscanf(file, "%s", name) != EOF) {
		fscanf(file, "%d", &price);
		// insert_element return: 1=success, 2=fail
		if(insert_element(&list, name, price) == 2) {
			printf("%s\n", "Fail!");
		}
	}
	fclose(file);

	while(1) {
		printf("%s", "1 print_list, 2 delete_element, 3 exit: ");
		scanf("%d", &input);
		switch(input) {
			case 1: 						// print_list
				print_list(list);
				break;
			case 2:							// delete_element return: 1=exist, 2=!exist
				printf("%s", "delete element: ");
				scanf("%s", name);
				if(delete_element(&list, name) == 1) {
					printf("%s\n", "delete success");
				} else if(delete_element(&list, name) == 2) {
					printf("%s\n", "not exist");
				}
				break;
			case 3:							// destory_list & exit
				destory_list(&list);
				// system("pause");
				return;
		}
	}
}
