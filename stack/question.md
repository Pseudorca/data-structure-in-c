﻿﻿##利用Stack以及Dynamic memory allocation完成下列程式設計
---
- 產生兩個stack, stack-even 用來儲存正偶數, stack-odd 用來儲存正奇數
- 當使用者輸入正偶數時則將該正偶數push進入stack-even, 若是輸入值為正奇數時則將該值push進入stack-odd
- 若使用者輸入為-1時, 若stack-odd為empty stack則顯示"stack odd empty", 否則則由stack-odd pop出一個數字, 並列印於螢幕上
- 若使用者輸入為-2時, 若stack-even為empty stack則顯示"stack even empty", 否則則由stack-even pop出一個數字, 並列印於螢幕上
- 若使用者輸入為0, 則依序由兩個stack pop出值並列印於螢幕上, pop的順序為兩個stack頂端之值為大者先輸出, 最後當兩個stack皆為empty後則destory此兩個stacks, 然後結束程式。
