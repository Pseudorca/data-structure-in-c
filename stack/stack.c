#include <stdio.h>
#include <stdlib.h>

struct Stack {
	int element;
	struct Stack *next;
};

void push(struct Stack** root, int in){
	struct Stack* node = (struct Stack*)malloc(sizeof(struct Stack));	// add new node
	node->element = in;							// set value
	node->next = *root;
	*root = node;								// update root
}

int pop(struct Stack** root) {
	struct Stack* tmp = *root;
	int out = tmp->element;
	*root = tmp->next;	// update root
	free(tmp);		// free memory

	return out;
}

int main() {
	int in;
	struct Stack* even = NULL; 
	struct Stack* odd = NULL;

	while(scanf("%d", &in)){
		if(in > 0) {
			if(in%2 == 0) {
				push(&even, in);
			} else if(in%2 == 1) {
				push(&odd, in);
			}
		} else if(in == 0) {
			while(odd != NULL || even != NULL) {
				if(odd == NULL) {
					printf("%d\n", pop(&even));
				}
				else if(even == NULL) {
					printf("%d\n", pop(&odd));
				}
				else{
					if(odd->element >= even->element)
						printf("%d\n", pop(&odd));
					else
						printf("%d\n", pop(&even));
				}
			}
			break;
		} else {
			if(in == -1) {
				if(odd == NULL)
					printf("stack odd empty\n");
				else
					printf("%d\n", pop(&odd));
			} else if(in == -2) {
				if(even == NULL)
					printf("stack even empty\n");
				else
					printf("%d\n", pop(&even));
			}
		}
	}
	// system("pause");
	return 0;
}
